<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

namespace RedisBackup;

use Predis\Client;

class RedisFactory {
    /** @var Client[] */
    protected static $instances = array();

    /**
     * @param string $host
     * @param int $port
     * @param null|string $password
     * @param int $database
     * @return Client
     */
    public static function instance($host, $port, $password=null, $database=0) {
        $key = hash("md5", $host.$port.$password.$database);
        if (!isset(self::$instances[$key])) {
            $params = array(
                "host" => $host,
                "port" => $port
            );
            if ($password != null) {
                $params['password'] = $password;
            }
            Logger::D("Connecting to redis with $host:$port " . ($password != null ? "with password" : "default credentials") . " DB: $database");
            self::$instances[$key] = new Client($params);
            self::$instances[$key]->select($database);
        }
        Logger::D("Getting redis instance $key on $host:$port/$database");
        return self::$instances[$key];
    }
}