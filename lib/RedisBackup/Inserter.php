<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

namespace RedisBackup;

class Inserter {

    protected $redis = null;
    protected $started = false;
    protected $closed = false;
    protected $filters = array();
    protected $counter = 0;

    /**
     * @param string $host
     * @param int $port
     * @param string|null $password
     * @param int $database
     */
    public function __construct($host, $port, $password=null, $database=0) {
        $this->redis = RedisFactory::instance($host, $port, $password, $database);
    }
    public function __destruct() {
        if (!$this->closed) {
            Logger::W("Failed to close Inserter");
            $this->close();
        }
    }

    protected function addFilter($filter) {
        if ($this->started) {
            throw new \Exception("Trying to add filter $filter to a started inserter");
        }
        $this->filters[$filter] = $this->counter++;
    }

    public function insert(Record $r) {
        if ($this->closed) {
            throw new \Exception("Tried to write to a closed inserter");
        }
        if (!$this->started) {
            $this->started = true;
            $this->filters = array_keys($this->filters);
            $size = $this->redis->dbsize();
            if ($size > 0) {
                throw new \Exception("Cannot insert into database, it has existing items");
            }
        }
        foreach($this->filters as $f) {
            if (preg_match("/$f/", $r->key) !== false) {
                Logger::D("Skipping insert of ".$r->key." because of filter $f");
                return;
            }
        }
        Logger::D("Restoring ".$r->key." with ".$r->ttl.' TTL');
        $res = $this->redis->restore($r->key, $r->ttl, $r->value);
        if ($res != "OK") {
            throw new \Exception("Error restoring ".$r->key);
        }
    }

    public function close() {
        $this->closed = true;
        unset($this->redis);
        Logger::D("Closed Inserter");
    }
}