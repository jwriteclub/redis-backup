<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

namespace RedisBackup;

class FileWriter
{

    const WRITER_VERSION = "1";

    protected $fhandle;
    protected $closed = true;

    public function __construct($file = "stdout")
    {
        if ($file == "stdout") {
            $this->fhandle = $this->fopen("php://stdout");
        } else {
            $this->fhandle = $this->fopen($file);
        }
        if ($this->fhandle === false) {
            throw new \Exception("Unable to open file $file");
        }
        $this->closed = false;
        Logger::D("Opened file $file");
        $this->writedata("RedisBackup v" . self::WRITER_VERSION . "\r\n"); // 16 bytes long, for better alignment
    }

    public function __destruct()
    {
        if (!$this->closed) {
            Logger::W("Failed to close writer. Data may be incomplete");
            $this->close();
        }
    }

    protected function writedata($data)
    {
        if ($this->closed) {
            throw new \Exception("Tried to write to a closed file");
        }
        $res = $this->fwrite($data);
        if ($res === false) {
            throw new \Exception("Error writing data to file");
        }
    }

    protected function fopen($location)
    {
        return fopen($location, "wb");
    }

    protected function fwrite($data)
    {
        return fwrite($this->fhandle, $data);
    }

    protected function fclose()
    {
        return fclose($this->fhandle);
    }

    public function write(Record $r)
    {
        Logger::D("Writing record " . $r->key);
        $this->writedata($r->serialize());
    }

    public function close()
    {
        $res = $this->fclose();
        if (!$res) {
            Logger::W("Error closing output file");
        }
        $this->closed = true;
        Logger::D("Closed output file");
    }

}