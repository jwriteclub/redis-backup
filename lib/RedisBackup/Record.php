<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

namespace RedisBackup;

class Record {

    const FLAGS_VERSION_STRING = 0b0101000000000000;
    const FLAGS_HAS_TTL = 0b0000000000000001;

    const FLAGS_TLEN_16_BIT = 0b0000000000000010;
    const FLAGS_TLEN_32_BIT = 0b0000000000000100;
    const FLAGS_TLEN_64_BIT = 0b0000000000001000;

    const FLAGS_KLEN_16_BIT = 0b0000000000010000;
    const FLAGS_KLEN_32_BIT = 0b0000000000100000;
    const FLAGS_KLEN_64_BIT = 0b0000000001000000;

    const FLAGS_DLEN_16_BIT = 0b0000000100000000;
    const FLAGS_DLEN_32_BIT = 0b0000001000000000;
    const FLAGS_DLEN_64_BIT = 0b0000010000000000;

    const MAX_VALUE_16_BIT = 0xFFFE;
    const MAX_VALUE_32_BIT = 0xFFFFFFFE;
    const MAX_VALUE_64_BIT = 0xFFFFFFFFFFFFFFFE;

    public $key;
    public $ttl = 0;
    public $value;

    public function serialize() {

        // Record format:
        // FLAGS[16] KEYLEN[16|32] TTL[16|32|64]? DATALEN[16|32] KEY DATA

        $flags = 0;
        $format = "n";
        // Set verion string
        $flags |= self::FLAGS_VERSION_STRING;

        $klen = strlen($this->key);
        if ($klen < self::MAX_VALUE_16_BIT) {
            $flags |= self::FLAGS_KLEN_16_BIT;
            $format .= "n";
        } else if ($klen < self::MAX_VALUE_32_BIT) {
            $flags |= self::FLAGS_KLEN_32_BIT;
            $format .= "N";
        } else {
            throw new \Exception("Value is longer than 2^64 characters"); // @codeCoverageIgnore
        }

        if ($this->ttl > 0) {
            $flags |= self::FLAGS_HAS_TTL;
            if ($this->ttl < self::MAX_VALUE_16_BIT) {
                $flags |= self::FLAGS_TLEN_16_BIT;
                $format .= "n";
            } else if ($this->ttl < self::MAX_VALUE_32_BIT) {
                $flags |= self::FLAGS_TLEN_32_BIT;
                $format .= "N";
            } else if ($this->ttl < self::MAX_VALUE_64_BIT) {
                $flags |= self::FLAGS_TLEN_64_BIT;
                $format .= "J";
            } else {
                throw new \Exception("TTL is bigger than 64 bits"); // @codeCoverageIgnore
            }
        }

        $vlen = strlen($this->value);
        if ($vlen < self::MAX_VALUE_16_BIT) {
            $flags |= self::FLAGS_DLEN_16_BIT;
            $format .= "n";
        } else if ($vlen < self::MAX_VALUE_32_BIT) {
            $flags |= self::FLAGS_DLEN_32_BIT;
            $format .= "N";
        } else {
            throw new \Exception("Value is longer than 2^64 characters"); // @codeCoverageIgnore
        }

        if ($this->ttl > 0) {
            $head = pack($format, $flags, $klen, $this->ttl, $vlen);
        } else {
            $head = pack($format, $flags, $klen, $vlen);
        }

        $hashctx = hash_init("crc32b");
        hash_update($hashctx, $head);
        hash_update($hashctx, $this->key);
        hash_update($hashctx, $this->value);
        $hash = hash_final($hashctx, true);

        return $head.$this->key.$this->value.$hash;
    }

    public static function deserialize(AReader $r) {
        Logger::D("Deserializing a record");

        $hashctx = hash_init("crc32b");

        $head = $r->readBytes(2);
        if ($head == null) {
            Logger::I("Error reading head");
            return null;
        }
        hash_update($hashctx, $head);

        $head = unpack("nhead", $head);
        $head = $head["head"];

        //Logger::D("Got header $head");

        if (!($head & self::FLAGS_VERSION_STRING)) {
            throw new \Exception("Unknown record version string");
        }

        $format = "";
        $toRead = 0;
        if ($head & self::FLAGS_KLEN_16_BIT) {
            $format .= "nklen";
            $toRead += 2;
        } else if ($head & self::FLAGS_KLEN_32_BIT) {
            $format .= "Nklen";
            $toRead += 4;
        } else {
            throw new \Exception("Key length specifier missing");
        }

        $hasTtl = false;
        if ($head & self::FLAGS_HAS_TTL) {
            Logger::D("Record has TTL");
            $hasTtl = true;
            if ($head & self::FLAGS_TLEN_16_BIT) {
                $format .= "/nttl";
                $toRead += 2;
            } else if ($head & self::FLAGS_TLEN_32_BIT) {
                $format .= "/Nttl";
                $toRead += 4;
            } else if ($head & self::FLAGS_TLEN_64_BIT) {
                $format .= "/Jttl";
                $toRead += 8;
            } else {
                throw new \Exception("TTL length specifier missing");
            }
        }

        if ($head & self::FLAGS_DLEN_16_BIT) {
            $format .= "/nvlen";
            $toRead += 2;
        } else if ($head & self::FLAGS_DLEN_32_BIT) {
            $format .= "/Nvlen";
            $toRead += 4;
        } else {
            throw new \Exception("Data length specifier missing");
        }

        //Logger::D("Reading $toRead bytes as $format");
        $headers = $r->readBytes($toRead);
        if ($headers == null) {
            throw new \Exception("Error reading record header. Input may be corrupted");
        }
        hash_update($hashctx, $headers);
        $headers = unpack($format, $headers);

        $ret = new Record();
        if ($hasTtl) {
            $klen = $headers["klen"];
            $ret->ttl = $headers["ttl"];
            $vlen = $headers["vlen"];
        } else {
            $klen = $headers["klen"];
            $ret->ttl = 0;
            $vlen = $headers["vlen"];
        }

        //Logger::D("Reading $klen bytes for the key");
        $ret->key = $r->readBytes($klen);
        if ($ret->key === null) {
            throw new \Exception("Error reading key name. Input may be corrupted");
        }
        hash_update($hashctx, $ret->key);
        Logger::D("Got key ".$ret->key);

        Logger::D("Reading $vlen bytes for the value");
        $ret->value = $r->readBytes($vlen);
        if ($ret->value === null) {
            throw new \Exception("Error reading value. Input may be corrupted");
        }
        hash_update($hashctx, $ret->value);

        $checksum = $r->readBytes(4);
        if ($checksum === null) {
            throw new \Exception("Error reading checksum. Input may be corrupted");
        }
        $hash = hash_final($hashctx, true);
        if ($checksum != $hash) {
            throw new \Exception("Invalid checksum. Input may be corrupted");
        }

        //Logger::D("Successfully deserialized a record");
        return $ret;
    }

}