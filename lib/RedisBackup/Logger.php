<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

namespace RedisBackup;

use Bramus\Monolog\Formatter\ColoredLineFormatter;
use Monolog\Handler\StreamHandler;

/**
 * Class Logger
 * @package RedisBackup
 */
class Logger {

    protected $log;

    protected static $instance;

    protected function __construct() {
        $this->log = new \Monolog\Logger("redis-backup");
    }

    public static function setLogLevel($level) {
        while(count(self::instance()->log->getHandlers()) > 0) {
            self::instance()->log->popHandler();
        }
        $handler = new StreamHandler("php://stderr", $level);
        $handler->setFormatter(new ColoredLineFormatter());
        self::instance()->log->pushHandler($handler);
        self::D("Set log level to $level");
    }

    public static function instance() {
        if (self::$instance == null) {
            self::$instance = new Logger();
        }
        return self::$instance;
    }

    public static function D($msg) {
        self::instance()->log->addDebug($msg);
    }

    public static function I($msg) {
        self::instance()->log->addInfo($msg);
    }

    public static function W($msg) {
        self::instance()->log->addWarning($msg);
    }

    public static function E($msg) {
        self::instance()->log->addError($msg);
    }

}