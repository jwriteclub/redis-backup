<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

namespace RedisBackup;


class FileReader extends AReader
{

    const READER_VERSION = "1";

    protected $closed = true;
    protected $fhandle;

    public function __construct($file = "stdin")
    {
        if ($file == "stdin") {
            $this->fhandle = $this->fopen("php://stdin");
        } else {
            $this->fhandle = $this->fopen($file);
        }
        if ($this->fhandle === false) {
            throw new \Exception("Unable to open file $file");
        }
        $this->closed = false;
        Logger::D("Opened file $file");
        $verString = "RedisBackup v" . self::READER_VERSION . "\r\n";

        $readString = $this->readBytes(strlen($verString));
        if ($readString != $verString) {
            throw new \Exception("Unknown RedisBackup version file detected. Please use a newer version of RedisBackup to read this file. " . $readString . '<=>' . $verString);
        }
    }

    public function readBytes($bytes)
    {
        if ($this->closed) {
            throw new \Exception("Tried to read from a closed file");
        }
        $res = $this->fread($bytes);
        if ($res === false || strlen($res) != $bytes) {
            return null;
        }
        return $res;
    }

    public function close()
    {
        $res = $this->fclose();
        if (!$res) {
            Logger::W("Error closing input file");
        }
        $this->closed = true;
        Logger::D("Closed input file");
    }

    protected function fopen($location)
    {
        return fopen($location, "rb");
    }

    protected function fread($bytes)
    {
        return fread($this->fhandle, $bytes);
    }

    protected function fclose()
    {
        return fclose($this->fhandle);
    }
}