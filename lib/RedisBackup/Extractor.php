<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

namespace RedisBackup;

/*
 * Implementation note: This file takes advantage of the hash table like nature
 * of PHP array keys to avoid duplicates in the set of selectors, filters and keys.
 * That is, this code inserts the incoming items into the arrays as keys, and then
 * uses the `array_keys` function to gather the unique keys just before actually
 * using them, to avoid repeated calls to `in_array` function.
 */
class Extractor {

    protected $redis = null;
    protected $started = false;
    protected $closed = false;
    protected $keyset = null;

    /**
     * @param string $host
     * @param int $port
     * @param string|null $password
     * @param int $database
     */
    public function __construct($host, $port, $password=null, $database=0) {
        $this->redis = RedisFactory::instance($host, $port, $password, $database);
        $this->keyset = new KeySet($host, $port, $password, $database);
    }

    public function __destruct() {
        if (!$this->closed) {
            Logger::W("Failed to close extractor");
            $this->close();
        }
    }

    /**
     * Adds a positive selector used to generate the list of
     * keys which are backed up.
     * @param string $selector
     * @throws \Exception
     */
    public function addSelector($selector) {
        $this->keyset->addSelector($selector);
    }

    /**
     * Adds a regular expression used to negatively filter possible keys
     * out of the set of keys included in the backup.
     * @param string $filter
     * @throws \Exception
     */
    public function addFilter($filter) {
        $this->keyset->addFilter($filter);
    }

    /**
     * Reads a record from redis.
     * @return null|Record The record or null if there's no more records available
     * @throws \Exception
     */
    public function readEntry() {
        if ($this->closed) {
            throw new \Exception("Unable to read from a closed extractor");
        }
        do {
            $key = $this->keyset->getKey();
            if ($key == null) {
                return null;
            }
            $r = new Record();
            Logger::D("Fetching $key");

            $r->key = $key;
            $r->value = $this->redis->dump($key);
            if ($r->value == null) {
                $r = null;
                Logger::E("Skipping $key as its empty");
                continue;
            }

            $r->ttl = $this->redis->pttl($key);
            if (!is_int($r->ttl) || $r->ttl < 0) {
                $r->ttl = 0;
            }
        } while($r == null);
        return $r;
    }

    /**
     * @generate Record
     * @return \Generator
     */
    public function generateRecords() {
        while(($r = $this->readEntry()) !== null) {
            yield $r;
        }
    }

    public function close() {
        $this->closed = true;
        unset($this->keyset);
        unset($this->redis);
        Logger::D("Closed Extractor");
    }

}