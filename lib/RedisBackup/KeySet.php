<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

namespace RedisBackup;

class KeySet {

    protected $redis;
    protected $started = false;
    protected $selectors = array();
    protected $filters = array();
    protected $keys = array();
    protected $kpos = 0;
    protected $counter = 0;

    public function __construct($host, $port, $password=null, $database=0) {
        $this->redis = RedisFactory::instance($host, $port, $password, $database);
    }

    /**
     * Adds a positive selector used to generate the list of
     * keys which are backed up.
     * @param string $selector
     * @throws \Exception
     */
    public function addSelector($selector) {
        if ($this->started) {
            throw new \Exception("Unable to add a selector to a started or closed Extractor");
        }
        $this->selectors[$selector] = $this->counter++;
    }

    /**
     * Adds a regular expression used to negatively filter possible keys
     * out of the set of keys included in the backup.
     * @param string $filter
     * @throws \Exception
     */
    public function addFilter($filter) {
        if ($this->started) {
            throw new \Exception("Unable to add a filter to a started Extractor");
        }
        $this->filters[$filter] = $this->counter++;
    }

    public function getKey() {
        if (!$this->started) {
            $this->started = true;
            $this->selectors = array_keys($this->selectors);
            foreach($this->selectors as $selector) {
                $keys = $this->redis->keys($selector);
                if ($keys == null || !is_array($keys) || count($keys) < 1) {
                    Logger::W("No keys generated from selector $selector");
                    continue;
                }
                foreach($keys as $k) {
                    $this->keys[$k] = $this->counter++;
                }
                Logger::D("Added ".count($keys)." keys from $selector");
            }
            $this->filters = array_keys($this->filters);
            $this->keys = array_keys($this->keys);
        }
        do {
            if ($this->kpos == count($this->keys)) {
                return null; // bail out if there's no more keys
            }
            $key = $this->keys[$this->kpos];
            $this->kpos += 1;
            foreach ($this->filters as $filter) {
                Logger::D("Applying filter $filter");
                if (preg_match("/$filter/", $key) !== false) {
                    Logger::D("Skipping key $key due to filter $filter");
                    $k = null;
                    continue 2;
                }
            }
        } while($key == null);
        return $key;
    }

    public function generateKeys() {
        while(($key = $this->getKey()) !== null) {
            yield $key;
        }
    }

}