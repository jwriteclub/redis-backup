<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

namespace RedisBackup;

class Docopt {

    const DOC_OPT = <<<DOC
Usage:
  redis-backup.phar backup [-vzq] [-h hostname] [-p port] [-d database] [-a password] [-o <file>]
  redis-backup.phar restore [-vzq] [-h hostname] [-p port] [-d database] [-a password] [-i <file>]
  redis-backup.phar migrate [-vq]
                            [--input-hostname hostname] [--input-port port] --input-database database [--input-password password]
                            [--output-hostname hostname] [--output-port port] --output-database database [--output-password password]
  redis-backup.phar size [-vq] [-h hostname] [-p port] [-a password]
  redis-backup.phar keyspace [-vq] [-h hostname] [-p port] [-d database] [-a password] [-s seperator]
  redis-backup.phar version [-vq]

Options:
  --help                               Show this
  -v --verbose                         Lots of logging
  -q --quiet                           Minimal logging
  -z --gzip                            Work with gzipped input/output
  -h hostname, --hostname hostname     Hostname or ip address [default: 127.0.0.1]
  -p port, --port port                 Port [default: 6379]
  -d database, --database database     Database number [default: 0]
  -a password, --password password     Password [default: ""]
  -i infile, --input infile            Input filename (for restore) [default: stdin]
  -o outfile, --output outfile         Output filename (for backup) [default: stdout]
  -s seperator, --seperator seperator  Keyspace seperator (for keyspace) [default: :]
  --input-hostname hostname            Input hostname (for migration) [default: 127.0.0.1]
  --input-port port                    Input port (for migration) [default: 6379]
  --input-database database            Input database number (for migration)
  --input-password password            Input password (for migration) [default: ""]
  --output-hostname hostname           Output hostname (for migration) [default: 127.0.0.1]
  --output-port port                   Output port (for migration) [default: 6379]
  --output-database database           Output database number (for migration)
  --output-password password           Output password (for migration) [default: ""]

DOC;

    const MODE_BACKUP = "backup";
    const MODE_RESTORE = "restore";
    const MODE_MIGRATE = "migrate";
    const MODE_SIZE = "size";
    const MODE_KEYSPACE = "keyspace";
    const MODE_VERSION = "version";

    const TYPE_INPUT = "input";
    const TYPE_OUTPUT = "output";

    protected $args;

    public function __construct() {
        $this->args = \Docopt::handle(self::DOC_OPT);
        if ($this->args->args['--quiet'] == true) {
            Logger::setLogLevel(\Monolog\Logger::ERROR);
        } else if ($this->args->args['--verbose'] == true) {
            Logger::setLogLevel(\Monolog\Logger::DEBUG);
        } else {
            Logger::setLogLevel(\Monolog\Logger::INFO);
        }
    }

    public function isValid() {
        return $this->args->status === 0;
    }

    public function mode() {
        if ($this->args->args['backup']) return self::MODE_BACKUP;
        if ($this->args->args['restore']) return self::MODE_RESTORE;
        if ($this->args->args['migrate']) return self::MODE_MIGRATE;
        if ($this->args->args['size']) return self::MODE_SIZE;
        if ($this->args->args['keyspace']) return self::MODE_KEYSPACE;
        if ($this->args->args['version']) return self::MODE_VERSION;
        return "unknown";
    }

    public function output() {
        return $this->args->args['--output'];
    }
    public function input() {
        return $this->args->args["--input"];
    }

    public function seperator() {
        return $this->args->args['--seperator'];
    }

    public function gzip() {
        return $this->args->args['--gzip'];
    }

    protected function getConnectionVariable($type, $name) {
        if ($type != self::TYPE_INPUT && $type != self::TYPE_OUTPUT) {
            throw new \Exception("Invalid type $type");
        }

        if ($this->mode() != self::MODE_MIGRATE) {
            return $this->args->args['--'.$name];
        } else {
            return $this->args->args['--'.$type.'-'.$name];
        }
    }
    public function hostname($type) {
        return $this->getConnectionVariable($type, "hostname");
    }
    public function port($type) {
        return $this->getConnectionVariable($type, "port");
    }
    public function database($type) {
        return $this->getConnectionVariable($type, "database");
    }
    public function password($type) {
        $ret = $this->getConnectionVariable($type, "password");
        if ($ret == "") {
            return null;
        }
        return $ret;
    }

}