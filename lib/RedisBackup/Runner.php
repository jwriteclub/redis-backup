<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

namespace RedisBackup;

class Runner {

    public static function run() {
        $opts = new Docopt();
        if (!$opts->isValid()) {
            Logger::E("Invalid options");
        }
        if ($opts->mode() == Docopt::MODE_BACKUP) {
            Logger::I("Performing a backup");
            try {
                if ($opts->gzip()) {
                    $writer = new GzipFileWriter($opts->output());
                } else {
                    $writer = new FileWriter($opts->output());
                }

                $extractor = new Extractor(
                    $opts->hostname(Docopt::TYPE_INPUT),
                    $opts->port(Docopt::TYPE_INPUT),
                    $opts->password(Docopt::TYPE_INPUT),
                    $opts->database(Docopt::TYPE_INPUT)
                );
                $extractor->addSelector("*");

                $written = 0;
                foreach($extractor->generateRecords() as $r) {
                    $writer->write($r);
                    $written += 1;
                }

                $extractor->close();
                $writer->close();

                Logger::D("Wrote $written records");
            } catch (\Exception $ex) {
                Logger::D("Error backing up");
                Logger::E($ex->getMessage());
            }
        } else if ($opts->mode() == Docopt::MODE_RESTORE) {
            Logger::I("Performing a restore");
            try {
                if ($opts->gzip()) {
                    $reader = new GzipFileReader($opts->input());
                } else {
                    $reader = new FileReader($opts->input());
                }

                $inserter = new Inserter(
                    $opts->hostname(Docopt::TYPE_OUTPUT),
                    $opts->port(Docopt::TYPE_OUTPUT),
                    $opts->password(Docopt::TYPE_OUTPUT),
                    $opts->database(Docopt::TYPE_OUTPUT)
                );

                $read = 0;
                foreach($reader->generateRecords() as $r) {
                    Logger::D("Got record ".$r->key." [".$r->ttl."]");
                    $inserter->insert($r);
                    Logger::D("Inserted record ".$r->key);
                    $read += 1;
                }

                $reader->close();
                $inserter->close();

                Logger::D("Read $read records");
            } catch(\Exception $ex) {
                Logger::D("Error restoring");
                Logger::E($ex->getMessage());
            }
        } else if ($opts->mode() == Docopt::MODE_MIGRATE) {
            try {
                Logger::D("Migrating");

                $extractor = new Extractor(
                    $opts->hostname(Docopt::TYPE_INPUT),
                    $opts->port(Docopt::TYPE_INPUT),
                    $opts->password(Docopt::TYPE_INPUT),
                    $opts->database(Docopt::TYPE_INPUT)
                );
                $extractor->addSelector("*");
                $inserter = new Inserter(
                    $opts->hostname(Docopt::TYPE_OUTPUT),
                    $opts->port(Docopt::TYPE_OUTPUT),
                    $opts->password(Docopt::TYPE_OUTPUT),
                    $opts->database(Docopt::TYPE_OUTPUT)
                );

                $processed = 0;
                foreach ($extractor->generateRecords() as $r) {
                    $inserter->insert($r);
                    $processed += 1;
                }

                $extractor->close();
                $inserter->close();

                Logger::D("Migrated $processed records");
            } catch (\Exception $ex) {
                Logger::D("Error migrating");
                Logger::E($ex->getMessage());
            }
        } else if ($opts->mode() == Docopt::MODE_SIZE) {
            Logger::I("Computing DB size information");
            $db = 0;
            $redis = RedisFactory::instance(
                $opts->hostname(Docopt::TYPE_INPUT),
                $opts->port(Docopt::TYPE_INPUT),
                $opts->password(Docopt::TYPE_INPUT)
            );
            echo " ID "."   SIZE   "." COMMENT ".PHP_EOL;
            while(true) {
                try {
                    $redis->select($db);
                } catch(\Exception $ex) {
                    break;
                }
                echo str_pad($db, 3, " ", STR_PAD_LEFT)." ";
                $size = $redis->dbsize();
                echo str_pad(number_format($size), 9, " ", STR_PAD_LEFT)." ";
                $comment = $redis->get(".dbcomment");
                if ($comment != null && is_string($comment)) {
                    echo str_pad($comment, 10, " ");
                } else {
                    echo str_pad("", 10, " ");
                }
                echo PHP_EOL;
                $db += 1;
            }
        } else if ($opts->mode() == Docopt::MODE_KEYSPACE) {
            Logger::i("Exploring keyspace");
            $keyset = new KeySet(
                $opts->hostname(Docopt::TYPE_INPUT),
                $opts->port(Docopt::TYPE_INPUT),
                $opts->password(Docopt::TYPE_INPUT),
                $opts->database(Docopt::TYPE_INPUT)
            );
            $keyset->addSelector("*");
            $db = array("keycount" => 0, "localkeys" => 0, "subkeys" => array());
            $seperator = $opts->seperator();
            $counter = 0;
            function exploreKey($key, &$database, &$counter) {
                $local = array_shift($key);
                $database["keycount"] += 1;
                if (count($key) > 0) {
                    if (!isset($database['subkeys'][$local])) {
                        $database['subkeys'][$local] = array("keycount" => 0, "localkeys" => 0, "subkeys" => array());
                    }
                    exploreKey($key, $database['subkeys'][$local], $counter);
                } else {
                    $database['localkeys'] += 1;
                }
                $counter += 1;
                if ($counter % 1000 === 0) {
                    Logger::D("Processed $counter keys");
                }
            }
            foreach($keyset->generateKeys() as $key) {
                $key = explode($seperator, $key);
                exploreKey($key, $db, $counter);
            }
            $output = array();
            function buildResults($key, $seperator, $database, &$out, &$maxlen) {
                $out[$key] = array("local" => $database['localkeys'], "total" => $database['keycount']);
                foreach($database['subkeys'] as $subkey => $subdb) {
                    buildResults($key.$seperator.$subkey, $seperator, $subdb, $out, $maxlen);
                }
                $maxlen = max(strlen($key), $maxlen);
            }
            $maxlen = 0;
            buildResults("<root>", $seperator, $db, $output, $maxlen);
            ksort($output);
            foreach($output as $key => $v) {
                echo str_pad($key, $maxlen + 1, " ").number_format($v['total'])." keys";
                if ($v['local'] > 0) {
                    echo " (" . number_format($v['local']) . " local)";
                }
                echo PHP_EOL;
            }
        } else if ($opts->mode() == Docopt::MODE_VERSION) {
            Logger::E("Version mode is not yet supported");
        } else {
            Logger::E("Invalid operation mode");
        }
    }

}