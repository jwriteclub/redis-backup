<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

/**
 * @covers \RedisBackup\StaticReader
 */
class StaticReaderTest extends PHPUnit_Framework_TestCase {

    public function testStaticReaderBasicRead() {
        $r = new \RedisBackup\StaticReader("12345");
        $this->assertEquals("1", $r->readBytes(1));
        $this->assertEquals("2", $r->readBytes(1));
        $this->assertEquals("3", $r->readBytes(1));
        $this->assertEquals("4", $r->readBytes(1));
        $this->assertEquals("5", $r->readBytes(1));
    }

    public function testStaticReaderLongRead() {
        $r = new \RedisBackup\StaticReader("12345");
        $this->assertEquals("12", $r->readBytes(2));
        $this->assertEquals("345", $r->readBytes(3));
    }

    public function testStaticReaderFullRead() {
        $r = new \RedisBackup\StaticReader("12345");
        $this->assertEquals("12345", $r->readBytes(5));
    }

    public function testStaticReaderOverRead() {
        $r = new \RedisBackup\StaticReader("12345");
        $this->assertNull($r->readBytes(6));
    }

    public function testStaticReaderTooManyReads() {
        $r = new \RedisBackup\StaticReader("12345");
        $this->assertEquals("1", $r->readBytes(1));
        $this->assertEquals("2", $r->readBytes(1));
        $this->assertEquals("3", $r->readBytes(1));
        $this->assertEquals("4", $r->readBytes(1));
        $this->assertEquals("5", $r->readBytes(1));
        $this->assertNull($r->readBytes(1));
    }

    public function testStaticReaderNoRead() {
        $r = new \RedisBackup\StaticReader("12345");
        $r = new \RedisBackup\StaticReader("12345");
        $this->assertEquals("", $r->readBytes(0));
        $this->assertEquals("1", $r->readBytes(1));
        $this->assertEquals("", $r->readBytes(0));
        $this->assertEquals("2", $r->readBytes(1));
        $this->assertEquals("", $r->readBytes(0));
        $this->assertEquals("3", $r->readBytes(1));
        $this->assertEquals("", $r->readBytes(0));
        $this->assertEquals("4", $r->readBytes(1));
        $this->assertEquals("", $r->readBytes(0));
        $this->assertEquals("5", $r->readBytes(1));
        $this->assertEquals("", $r->readBytes(0));
    }
}