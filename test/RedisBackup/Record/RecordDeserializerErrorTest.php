<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

/**
 * @covers \RedisBackup\Record::deserialize
 */
class RecordDeserializerErrorTest extends RecordTestCase {

    public function testShortHeader() {
        $re = new \RedisBackup\StaticReader("\x51");
        $r = \RedisBackup\Record::deserialize($re);
        $this->assertNull($r);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Unknown record version string
     */
    public function testInvalidHeader() {
        $re = new \RedisBackup\StaticReader("\x01\x10"."\x00\x04"."\x00\x06"."test"."record");
        $r = \RedisBackup\Record::deserialize($re);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Error reading record header. Input may be corrupted
     */
    public function testNoLengths() {
        $re = new \RedisBackup\StaticReader("\x51\x10");
        $r = \RedisBackup\Record::deserialize($re);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Error reading key name. Input may be corrupted
     */
    public function testInvalidKeyLenOverread() {
        $re = new \RedisBackup\StaticReader("\x51\x10"."\x00\x06"."\x00\x00"."test"."");
        $r = \RedisBackup\Record::deserialize($re);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Key length specifier missing
     */
    public function testMissingKeyLenSpecifier() {
        $re = new \RedisBackup\StaticReader("\x51\x00"."\x00\x04"."\x00\x00"."test"."");
        $r = \RedisBackup\Record::deserialize($re);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Error reading value. Input may be corrupted
     */
    public function testInvalidDataLenOverread() {
        $re = new \RedisBackup\StaticReader("\x51\x10"."\x00\x04"."\x00\x08"."test"."record");
        $r = \RedisBackup\Record::deserialize($re);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Data length specifier missing
     */
    public function testMissingDataLenSpecifier() {
        $re = new \RedisBackup\StaticReader("\x50\x10"."\x00\x04"."\x00\x00"."test"."");
        $r = \RedisBackup\Record::deserialize($re);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Error reading checksum. Input may be corrupted
     */
    public function testMissingChecksum() {
        $re = new \RedisBackup\StaticReader("\x51\x10"."\x00\x04"."\x00\x06"."test"."record");
        $r = \RedisBackup\Record::deserialize($re);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Invalid checksum. Input may be corrupted
     */
    public function testInvalidChecksum() {
        $re = new \RedisBackup\StaticReader("\x51\x10"."\x00\x04"."\x00\x06"."test"."record"."\x00\x00\x00\x00");
        $r = \RedisBackup\Record::deserialize($re);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage TTL length specifier missing
     */
    public function testMissingTtlLengthSpec() {
        $re = new \RedisBackup\StaticReader("\x51\x11"."\x00\x04"."\x00\x01"."\x00\x06"."test"."record");
        $r = \RedisBackup\Record::deserialize($re);
    }
}