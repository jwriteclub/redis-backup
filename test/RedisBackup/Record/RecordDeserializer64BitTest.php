<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

/**
 * @covers \RedisBackup\Record::deserialize
 */
class RecordDeserializer64BitTest extends RecordTestCase {

    public function testDeserializeNoKeyNoValue() {
        $re = new \RedisBackup\StaticReader($this->addHash("\x51\x19"."\x00\x00"."\x00\x00\x00\x01\x00\x00\x00\x01"."\x00\x00"."".""));
        $r = \RedisBackup\Record::deserialize($re);

        $this->assertNotNull($r);
        $this->assertEquals($r->key, "");
        $this->assertEquals($r->value, "");
        $this->assertEquals($r->ttl, pow(2,32)+1);
    }

}