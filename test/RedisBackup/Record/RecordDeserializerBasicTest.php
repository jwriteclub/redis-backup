<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

/**
 * @covers \RedisBackup\Record::deserialize
 */
class RecordDeserializerBasicTest extends RecordTestCase {
    public function testDeserializeNoTtlRecord() {
        $re = new \RedisBackup\StaticReader($this->addHash("\x51\x10"."\x00\x04"."\x00\x06"."test"."record"));
        $r = \RedisBackup\Record::deserialize($re);

        $this->assertNotNull($r);
        $this->assertEquals($r->key, "test");
        $this->assertEquals($r->value, "record");
        $this->assertEquals($r->ttl, 0);
    }

    public function testDeserializeNoTtlNoKeyRecord() {
        $re = new \RedisBackup\StaticReader($this->addHash("\x51\x10"."\x00\x00"."\x00\x06".""."record"));
        $r = \RedisBackup\Record::deserialize($re);

        $this->assertNotNull($r);
        $this->assertEquals($r->key, "");
        $this->assertEquals($r->value, "record");
        $this->assertEquals($r->ttl, 0);
    }

    public function testSerializeNoTtlNoValueRecord() {
        $re = new \RedisBackup\StaticReader($this->addHash("\x51\x10"."\x00\x04"."\x00\x00"."test".""));
        $r = \RedisBackup\Record::deserialize($re);

        $this->assertNotNull($r);
        $this->assertEquals($r->key, "test");
        $this->assertEquals($r->value, "");
        $this->assertEquals($r->ttl, 0);
    }

    public function testSerializeNoTtlNoKeyNoValueRecord() {
        $re = new \RedisBackup\StaticReader($this->addHash("\x51\x10"."\x00\x00"."\x00\x00"."".""));
        $r = \RedisBackup\Record::deserialize($re);

        $this->assertNotNull($r);
        $this->assertEquals($r->key, "");
        $this->assertEquals($r->value, "");
        $this->assertEquals($r->ttl, 0);
    }

    public function testDeserializeRecord() {
        $re = new \RedisBackup\StaticReader($this->addHash("\x51\x13"."\x00\x04"."\x00\x01"."\x00\x06"."test"."record"));
        $r = \RedisBackup\Record::deserialize($re);

        $this->assertNotNull($r);
        $this->assertEquals($r->key, "test");
        $this->assertEquals($r->value, "record");
        $this->assertEquals($r->ttl, 1);
    }

    public function testDeserializeNoKeyRecord() {
        $re = new \RedisBackup\StaticReader($this->addHash("\x51\x13"."\x00\x00"."\x00\x01"."\x00\x06".""."record"));
        $r = \RedisBackup\Record::deserialize($re);

        $this->assertNotNull($r);
        $this->assertEquals($r->key, "");
        $this->assertEquals($r->value, "record");
        $this->assertEquals($r->ttl, 1);
    }

    public function testSerializeNoValueRecord() {
        $re = new \RedisBackup\StaticReader($this->addHash("\x51\x13"."\x00\x04"."\x00\x01"."\x00\x00"."test".""));
        $r = \RedisBackup\Record::deserialize($re);

        $this->assertNotNull($r);
        $this->assertEquals($r->key, "test");
        $this->assertEquals($r->value, "");
        $this->assertEquals($r->ttl, 1);
    }

    public function testSerializeNoKeyNoValueRecord() {
        $re = new \RedisBackup\StaticReader($this->addHash("\x51\x13"."\x00\x00"."\x00\x01"."\x00\x00"."".""));
        $r = \RedisBackup\Record::deserialize($re);

        $this->assertNotNull($r);
        $this->assertEquals($r->key, "");
        $this->assertEquals($r->value, "");
        $this->assertEquals($r->ttl, 1);
    }
}