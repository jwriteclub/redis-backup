<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

/**
 * @covers \RedisBackup\Record::serialize
 */
class RecordSerializer64BitTests extends RecordTestCase {

    public function testSerializeNoKeyNoValueRecord() {
        $r = new \RedisBackup\Record();
        $r->key = "";
        $r->value = "";
        $r->ttl = pow(2, 32) + 1;

        $this->assertEquals($this->addHash("\x51\x19"."\x00\x00"."\x00\x00\x00\x01\x00\x00\x00\x01"."\x00\x00"."".""), $r->serialize());
    }
}