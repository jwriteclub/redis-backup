<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

/**
 * @covers \RedisBackup\Record::serialize
 */
class RecordSerializerBasicTests extends RecordTestCase {

    public function testSerializeNoTtlRecord() {
        $r = new \RedisBackup\Record();
        $r->key = "test";
        $r->value = "record";

        $this->assertEquals($this->addHash("\x51\x10"."\x00\x04"."\x00\x06"."test"."record"), $r->serialize());
    }

    public function testSerializeNoTtlNoKeyRecord() {
        $r = new \RedisBackup\Record();
        $r->key = "";
        $r->value = "record";

        $this->assertEquals($this->addHash("\x51\x10"."\x00\x00"."\x00\x06".""."record"), $r->serialize());
    }

    public function testSerializeNoTtlNoValueRecord() {
        $r = new \RedisBackup\Record();
        $r->key = "test";
        $r->value = "";

        $this->assertEquals($this->addHash("\x51\x10"."\x00\x04"."\x00\x00"."test".""), $r->serialize());
    }

    public function testSerializeNoTtlNoKeyNoValueRecord() {
        $r = new \RedisBackup\Record();
        $r->key = "";
        $r->value = "";

        $this->assertEquals($this->addHash("\x51\x10"."\x00\x00"."\x00\x00"."".""), $r->serialize());
    }

    public function testSerializeRecord() {
        $r = new \RedisBackup\Record();
        $r->key = "test";
        $r->value = "record";
        $r->ttl = 1;

        $this->assertEquals($this->addHash("\x51\x13"."\x00\x04"."\x00\x01"."\x00\x06"."test"."record"), $r->serialize());
    }

    public function testSerializeNoKeyRecord() {
        $r = new \RedisBackup\Record();
        $r->key = "";
        $r->value = "record";
        $r->ttl = 1;

        $this->assertEquals($this->addHash("\x51\x13"."\x00\x00"."\x00\x01"."\x00\x06".""."record"), $r->serialize());
    }

    public function testSerializeNoValueRecord() {
        $r = new \RedisBackup\Record();
        $r->key = "test";
        $r->value = "";
        $r->ttl = 1;

        $this->assertEquals($this->addHash("\x51\x13"."\x00\x04"."\x00\x01"."\x00\x00"."test".""), $r->serialize());
    }

    public function testSerializeNoKeyNoValueRecord() {
        $r = new \RedisBackup\Record();
        $r->key = "";
        $r->value = "";
        $r->ttl = 1;

        $this->assertEquals($this->addHash("\x51\x13"."\x00\x00"."\x00\x01"."\x00\x00"."".""), $r->serialize());
    }
}