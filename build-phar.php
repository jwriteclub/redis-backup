<?php
/*
 * This file is part of the redis-backup project, licensed under
 * the BSD open source license, which should have been included
 * along with this code, or may be accessed at the project's website
 * at https://bitbucket.org/jwriteclub/redis-backup
 *
 * Copyright (c) 2015 SendFaster, Inc.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contact: info@sendfaster.com
 *
 */

require_once("vendor/autoload.php");

$compiler = new Secondtruth\Compiler\Compiler(dirname(__FILE__));

$compiler->addIndexFile("redis-backup.php");
$compiler->addDirectory("lib");

$compiler->addFile("vendor/autoload.php");
$compiler->addDirectory("vendor/composer", "!*.php");

$compiler->addDirectory("vendor/bramus", ["*Test.php","examples/*","bootstrap.php","!*.php"]);
$compiler->addDirectory("vendor/docopt", ["*Test.php","examples/*","bootstrap.php","!*.php"]);
$compiler->addDirectory("vendor/monolog", ["*Test.php","examples/*","bootstrap.php","!*.php"]);
$compiler->addDirectory("vendor/predis", ["*Test.php","examples/*","bootstrap.php","!*.php"]);
$compiler->addDirectory("vendor/psr", ["*Test.php","examples/*","bootstrap.php","!*.php"]);

$compiler->compile(dirname(__FILE__)."/redis-backup.phar");
chmod(dirname(__FILE__)."/redis-backup.phar", 0755);
