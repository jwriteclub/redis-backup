# [Redis Backup](https://bitbucket.org/jwriteclub/redis-backup)

The redis backup program provides a set of tools for working with backing up, restoring and migrating redis databases
using the redis wire protocol. In addition, several useful methods are provided for exploring the keyspace of a redis
database and the databases on a sever.

## Status

Working:
- Backup
- Restore
- Keyspace Exploration
- Server scan
- Gzip files

In Progress:
- Granular selectors
- Granular filters
- Partial migration
- Force flags

## Building

To compile the phar, run `composer update` and then execute:

`php -dphar.readonly=off build-phar.php`

To run agains the code, just run `php redis-backup.php` in the code folder.

### Usage

`./redis-backup.phar --help`

```text
    Usage:
      redis-backup.phar backup [-vzq] [-h hostname] [-p port] [-d database] [-a password] [-o <file>]
      redis-backup.phar restore [-vzq] [-h hostname] [-p port] [-d database] [-a password] [-i <file>]
      redis-backup.phar migrate [-vq]
                                [--input-hostname hostname] [--input-port port] --input-database database [--input-password password]
                                [--output-hostname hostname] [--output-port port] --output-database database [--output-password password]
      redis-backup.phar size [-vq] [-h hostname] [-p port] [-a password]
      redis-backup.phar keyspace [-vq] [-h hostname] [-p port] [-d database] [-a password] [-s seperator]
      redis-backup.phar version [-vq]
    
    Options:
      --help                               Show this
      -v --verbose                         Lots of logging
      -q --quiet                           Minmal logging
      -z --gzip                            Work with gzipped input/output
      -h hostname, --hostname hostname     Hostname or ip address [default: 127.0.0.1]
      -p port, --port port                 Port [default: 6379]
      -d database, --database database     Database number [default: 0]
      -a password, --password password     Password [default: ""]
      -i infile, --input infile            Input filename (for restore) [default: stdin]
      -o outfile, --output outfile         Output filename (for backup) [default: stdout]
      -s seperator, --seperator seperator  Keyspace seperator (for keyspace) [default: :]
      --input-hostname hostname            Input hostname (for migration) [default: 127.0.0.1]
      --input-port port                    Input port (for migration) [default: 6379]
      --input-database database            Input database number (for migration)
      --input-password password            Input password (for migration) [default: ""]
      --output-hostname hostname           Output hostname (for migration) [default: 127.0.0.1]
      --output-port port                   Output port (for migration) [default: 6379]
      --output-database database           Output database number (for migration)
      --output-password password           Output password (for migration) [default: ""]
```

Both `migrate` and `restore` will refuse to overwrite an existing database.

### Backup

To backup a database, execute the `backup` command, either directing the output to a file, or specifying a file name. Optionally include
the `-z` flag to produce a gzipped output.

### Restore

To restore a database, execute the `restore` commend, either piping in a file or specifying a file name. If the file is in gzip format,
including the `-z` flag is mandatory.

### Migrate

To migrate between two databases, execute the `migrate` command. Migration will fail if the destination is not empty.

### Size

The `size` command will produce a listing of all of the databases on a particular redis server. For example:

```text
     ID    SIZE    COMMENT 
      0        40 General Testing Database
      1        40 General Testing Database
      2        40 General Testing Database
      3         0           
      4         0           
      5        51           
      6         3           
      7         0           
      8         0           
      9         0           
     10         0           
     11         0           
     12    99,288           
     13         0           
     14         0           
     15         0   
```

In the event that a string key called `.dbcomment` exists in the database, it will be displayed in the comment column.

### Keyspace

The `keyspace` comand will perform a scan of the keyspace breaking input keys on the specified seperator symbol (default ":"). For example:

```text
    <root>                     99,288 keys (1 local)
    <root>:asn                 1 keys
    <root>:asn:rev775a9434     1 keys (1 local)
    <root>:asninfo             1 keys (1 local)
    <root>:asnips              49,642 keys
    <root>:asnips:rev775a9434  49,642 keys (49,642 local)
    <root>:asnname             49,642 keys
    <root>:asnname:rev775a9434 49,642 keys (49,642 local)
    <root>:usedkeys            1 keys (1 local)
```

This can be extremely useful for exploring the contents of a redis database.

## Testing

Tests are powered by PHPUnit. There is a `phpunit.xml` file correctly configured in the project root, so simply running `phpunit` from the project root will execute the tests.

## Contributions

Contributions of additional actions, features or bug fixes are welcomed. Please make sure that all contributions contain relevant tests and open a pull request. By contributing you accept the MIT license for your code and agree to contribute your code irrevocably to SendFaster, Inc. Furthermore, you agree to hold SendFaster, Inc and it's employees harmless from any patent or copyright infringement contained in the code you contribute.

## Copyright

The Redis Backup program is licensed under the [MIT License](./LICENSE.md).